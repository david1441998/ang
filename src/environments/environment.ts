// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: true,
   firebaseConfig : {
    apiKey: "AIzaSyDsN7Er4BfhSkLqXEh7Db2yqr98mdhPPOA",
    authDomain: "bbc1-b3730.firebaseapp.com",
    databaseURL: "https://bbc1-b3730.firebaseio.com",
    projectId: "bbc1-b3730",
    storageBucket: "bbc1-b3730.appspot.com",
    messagingSenderId: "870304441957",
    appId: "1:870304441957:web:5b1117218e3af56448888c",
    measurementId: "G-Y7XWKZ9J5P"
  }}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
